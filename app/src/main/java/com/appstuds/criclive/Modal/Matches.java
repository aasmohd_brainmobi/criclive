package com.appstuds.criclive.Modal;


public class Matches {


    public FirstTeam getTeam1() {
        return team1;
    }

    public void setTeam1(FirstTeam team1) {
        this.team1 = team1;
    }

    public SecondTeam getTeam2() {
        return team2;
    }

    public void setTeam2(SecondTeam team2) {
        this.team2 = team2;
    }

    private  FirstTeam team1;
    private  SecondTeam team2;
    private  String match_number;
    private  String match_type;

    public String getMatch_status() {
        return match_status;
    }

    public void setMatch_status(String match_status) {
        this.match_status = match_status;
    }

    private  String match_status;


    public String getMatch_number() {
        return match_number;
    }

    public void setMatch_number(String match_number) {
        this.match_number = match_number;
    }

    public String getMatch_type() {
        return match_type;
    }

    public void setMatch_type(String match_type) {
        this.match_type = match_type;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getLive_match_url() {
        return live_match_url;
    }

    public void setLive_match_url(String live_match_url) {
        this.live_match_url = live_match_url;
    }

    private  String match_date;
    private  String live_match_url;

    public String getBroadcast_message() {
        return broadcast_message;
    }

    public void setBroadcast_message(String broadcast_message) {
        this.broadcast_message = broadcast_message;
    }

    private  String broadcast_message;


}
