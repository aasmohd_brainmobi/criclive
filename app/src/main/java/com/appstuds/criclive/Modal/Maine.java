package com.appstuds.criclive.Modal;

import java.util.ArrayList;



public class Maine {

    public ArrayList<Matches> getMatches() {
        return matches;
    }

    public void setMatches(ArrayList<Matches> matches) {
        this.matches = matches;
    }


    ArrayList<Matches> matches;
    ArrayList<Slider> slider;
    private String disclaimer;

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public Invite getInvitation() {
        return invitation;
    }

    public void setInvitation(Invite invitation) {
        this.invitation = invitation;
    }

    private Invite invitation;

    public ArrayList<Slider> getSlider() {
        return slider;
    }

    public void setSlider(ArrayList<Slider> slider) {
        this.slider = slider;
    }


}

