package com.appstuds.criclive.Activities;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.appstuds.criclive.Fragment.HomeFragment;
import com.appstuds.criclive.Interface.ItemClick;
import com.appstuds.criclive.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , ItemClick {

    String share;
    String disc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        // drawer view
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        HomeFragmentLaunch();
    }

    @Override
    public void passShare(String share)
    {
        this.share=share;

    }

    @Override
    public void passDisc(String Disc)
    {
        this.disc=Disc;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_search) {

            onShareAppSocially();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    private void onShareAppSocially() {
        String shareBody = share;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "CRIC LIVE TV Android App");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share CRIC LIVE TV via"));
    }


    private void openPlayStoreLink() {
        String appPackageName = this.getPackageName();
        // getPackageName() from Context or Activity object
        try {
            startActivity(
                    new Intent( Intent.ACTION_VIEW, Uri.parse
                            ("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException e){
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.play_store_base_url)
                    + appPackageName)));
        }
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.Share) {

          onShareAppSocially();


        } else if (id == R.id.Disclaimer) {

            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.custom);
            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            TextView textView=(TextView)dialog.findViewById(R.id.text_description);
            textView.setText(disc);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });
            dialog.show();


        } else if (id == R.id.Rateus) {

            openPlayStoreLink();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void HomeFragmentLaunch() {

        HomeFragment fragment = new HomeFragment();
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_main, fragment);
        ft.commit();
    }

    @Override
    public void clickAction(String url) {
        if(!url.equals("")) {


            Intent intent = new Intent(this, SliderWebActivity.class);
            intent.putExtra("Url", url);
            startActivity(intent);
        }
    }


}
