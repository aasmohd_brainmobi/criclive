package com.appstuds.criclive.Interface;

import com.appstuds.criclive.Modal.Feed;

import retrofit2.Call;
import retrofit2.http.GET;


public interface Api {

    String BASE_URL = "http://fantasy.apphosthub.com/pb/api/";

    @GET("getLiveMatches.php")
    Call<Feed> getResults();
}