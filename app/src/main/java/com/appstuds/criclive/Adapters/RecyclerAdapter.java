package com.appstuds.criclive.Adapters;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstuds.criclive.Activities.VideoActivity;
import com.appstuds.criclive.Modal.Matches;
import com.appstuds.criclive.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import cn.iwgang.countdownview.CountdownView;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    Context context;

    View view;
    SimpleDateFormat sdf;


    private List<Matches> my_data;


    public RecyclerAdapter(Context context, List<Matches> my_data) {

        this.context = context;

        this.my_data = my_data;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_adapterdata, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


  //      updateTimeRemaining(System.currentTimeMillis(), my_data.get(position).getMatch_date(), holder.match_date);
        holder.team1name.setText(my_data.get(position).getTeam1().getShort_name());
        holder.team2name.setText(my_data.get(position).getTeam2().getShort_name());


        Picasso.with(context).load(my_data.get(position).getTeam1().getLogo())
                .into(holder.team1flag);
        Picasso.with(context).load(my_data.get(position).getTeam2().getLogo())
                .into(holder.team2flag);

        String THorRD = getTHorRD(my_data.get(position).getMatch_number());

        String TimestToDate = getTStoDate(my_data.get(position).getMatch_date());

        String dateString = getLocalDate(Long.parseLong(my_data.get(position).getMatch_date())
                * 1000, "dd MMM yyyy");

        int imageribbon = ribbon(my_data.get(position).getMatch_type().toUpperCase());

        holder.ribbon.setImageResource(imageribbon);


        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

        Date date = null;
        try {
            date = sdf.parse(TimestToDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.match_type.setText(my_data.get(position).getMatch_number() + THorRD + my_data.get(position).getMatch_type().toUpperCase() + " MATCH");
       holder.match_date.setText(dateString);


        if ((my_data.get(position).getLive_match_url()) != "" && (my_data.get(position).getLive_match_url()) != null) {

            holder.live_match_url_textView.setVisibility(View.INVISIBLE);
            holder.live_match_button.setVisibility(View.VISIBLE);
        } else if ((my_data.get(position).getBroadcast_message()) != ""  && (my_data.get(position).getBroadcast_message()) != null) {
            holder.live_match_url_textView.setText(my_data.get(position).getBroadcast_message());

        } else if (TextUtils.isEmpty(my_data.get(position).getLive_match_url()) && TextUtils.isEmpty(my_data.get(position).getBroadcast_message()))
        {


            long df = getDuration(System.currentTimeMillis(),
                    Long.parseLong(my_data.get(position).getMatch_date()) * 1000);
            long dfff = df - 19800000;

            if (dfff > 24 * 60 * 60 * 1000) {
                holder.countdownView.setVisibility(View.INVISIBLE);
                holder.not_started.setVisibility(View.VISIBLE);
                holder.not_started.setText(my_data.get(position).getMatch_status());
            } else {
                holder.countdownView.setVisibility(View.VISIBLE);
                holder.countdownView.start(dfff);

            }

        }



        holder.live_match_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, VideoActivity.class);
                intent.putExtra("Url", my_data.get(position).getLive_match_url());
                context.startActivity(intent);


            }
        });



    }

    @Override
    public int getItemCount() {

        return my_data.size();

    }

    public void updateTimeRemaining(Long currentTime, String time, TextView holder) {
        Long timeDiff = new Date(java.lang.Long.parseLong(time) * 1000).getTime() - currentTime;
        timeDiff=timeDiff-19800000;
        if (timeDiff > 0) {
            Long seconds = timeDiff / 1000;
            Long hr = seconds / 3600;
            Long rem = seconds % 3600;
            Long mn = rem / 60;
            Long sec = rem % 60;

            String hrStr = "";
            if (hr < 10) {
                hrStr = "0" + hr;
            } else {
                hrStr = "" + hr;
            }

            String mnStr = "";
            if (mn < 10) {
                mnStr = "0" + mn;
            } else {
                mnStr = "" + mn;
            }

            String secStr = "";
            if (sec < 10) {
                secStr = "0" + sec;
            } else {
                secStr = "" + sec;
            }
            holder.setText(hrStr + " : " + mnStr + " : " + secStr);
        } else {
            holder.setText("past");
        }
    }


    public String getTHorRD(String value) {
        int valueint = Integer.parseInt(value);
        if (valueint == 1) return "ST ";
        else if (valueint == 2) return "ND ";
        else if (valueint == 3) return "RD ";
        else return "TH ";

    }

    public String getTStoDate(String tsValue) {


        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        Long num = Long.parseLong(tsValue);
        cal.setTimeInMillis(num * 1000);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        return date;

    }

    public long getDuration(long strDate, long timestToDate) {

        long diff = timestToDate - strDate;

        return diff;

    }

    public int ribbon(String value) {
        int[] image_resources = {R.drawable.t20_ribbon,
                R.drawable.odi_ribbon, R.drawable.test_ribbon};

        if (value.equals("T20")) return image_resources[0];
        else if (value.equals("ODI")) return image_resources[1];
        else if (value.equals("TEST")) return image_resources[2];
        else return 1;


    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView team1name, team2name, not_started, match_number, match_type, match_date, live_match_url_textView;//,release_date,ratingxml,moreInfoXml,secondapi;
        ImageView team1flag, team2flag, ribbon;
        Button live_match_button;
        CountdownView countdownView;


        public RecyclerViewHolder(View View) {
            super(View);
            team1flag = (ImageView) View.findViewById(R.id.team1_flag);
            team2flag = (ImageView) View.findViewById(R.id.team2_flag);
            team1name = (TextView) View.findViewById(R.id.team1_name);
            team2name = (TextView) View.findViewById(R.id.team2_name);
            match_type = (TextView) View.findViewById(R.id.match_no);//change it
            match_date = (TextView) View.findViewById(R.id.match_date);
            live_match_url_textView = (TextView) View.findViewById(R.id.watchnow_startin);
            live_match_button = (Button) View.findViewById(R.id.buttoncomego);
            countdownView = (CountdownView) View.findViewById(R.id.countdown_view);
            ribbon = (ImageView) View.findViewById(R.id.ribbon);
            not_started = (TextView) View.findViewById(R.id.not_started);


        }
    }

    public static String getLocalDate(long milliSeconds, String dateFormat) {
// Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getDefault(/*"GMT+5:30"*/));
        String dateString = formatter.format(new Date(milliSeconds));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));

// return formatter.format(calendar.getTime());
        return dateString;
    }


}
