package com.appstuds.criclive.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.appstuds.criclive.Interface.ItemClick;
import com.appstuds.criclive.Activities.MainActivity;
import com.appstuds.criclive.Modal.Slider;
import com.appstuds.criclive.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CustomSwipeAdapter extends PagerAdapter {


    private Context ctx;
    private LayoutInflater layoutInflater;
    List<Slider> sliderlist;
    String share;
    String disclaimer;
    ItemClick pass;


    public CustomSwipeAdapter(Context ctx, List<Slider> sliderlist, String share, String disclaimer) {
        this.ctx = ctx;
        this.sliderlist = sliderlist;
        this.disclaimer = disclaimer;
        this.share = share;


    }


    @Override
    public int getCount() {
        return sliderlist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return (view == (LinearLayout) o);

    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        layoutInflater = LayoutInflater.from(ctx);
        View item_view = layoutInflater.inflate(R.layout.swipe_layout, container, false);
        ImageView imageview = (ImageView) item_view.findViewById(R.id.image_view);

        container.addView(item_view);

        Picasso.with(ctx).load(sliderlist.get(position).getSlide())

                .into(imageview);

        pass = (ItemClick) ctx;
        pass.passShare(share);
        pass.passDisc(disclaimer);

        item_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pass = (ItemClick) ctx;
                pass.clickAction(sliderlist.get(position).getUrl());


            }
        });


        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }



}
